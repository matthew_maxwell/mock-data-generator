module.exports = {
  id: {
    type: "number",
    floor: true,
    unique: true,
    lessThan: 10000
  },
  type: ["Rule", "Proposed Rule"],
  title: "string",
  part: {
    type: "number",
    floor: true
  },
  effective_on: "date",
  text_content: {
    type: "string",
    length: 1000
  },
  abstract: {
    type: "string",
    length: 200
  },
  agencies: ["DEPARTMENT OF THE TREASURY", "Office of the Comptroller of the Currency", "DEPARTMENT OF AGRICULTURE", "Food Safety and Inspection Service"],
  significant: "bool",
  docket_ids: {
    type: "array",
    arrayType: "string",
    length: 2,
    arrayConstraints: {
      length: 5
    }
  },
  types: ["Bridges", "Mortgages", "Marine Safety", "Government Contracts"]
};