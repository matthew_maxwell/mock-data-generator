(function () {
  var generator = {
    generate: function (schema, amount) {
      var results = [],
        total = amount || 50,
        toString = Object.prototype.toString,
        uniques = {},
        randomGenerator = function (type, options) {
          var constraints = options || {},
            randomString = function (length, chars) {
              var mask = "",
                result = "",
                i = length;

              if (chars.indexOf("a") > -1) {
                mask += "abcdefghijklmnopqrstuvwxyz";
              }

              if (chars.indexOf("A") > -1) {
                mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
              }

              if (chars.indexOf("#") > -1) {
                mask += "0123456789";
              }

              if (chars.indexOf("!") > -1) {
                mask += "~`!@#$%^&*()_+-={}[]:\";\'<>?,./|\\";
              }

              while (i--) {
                result += mask[Math.round(Math.random() * (mask.length - 1))];
              }

              return result;
            },
            randomDate = function (start, end) {
              return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
            },
            length, arrayType, result;

          if (type == "number") {
            result = Math.random() * (constraints.lessThan || 100);

            if (constraints.negative) {
              result *= -1;
            }

            if (constraints.floor) {
              result = Math.floor(result);
            }

            if (constraints.ceiling) {
              result = Math.ceil(result);
            }

            if (constraints.round) {
              result = Math.round(result);
            }
          } else if (type === "string") {
            result = randomString(constraints.length || 500, constraints.chars || "#aA!")
          } else if (type === "date") {
            result = randomDate(constraints.start || new Date(2013, 0, 1), constraints.end || new Date());
          } else if (type === "array") {
            result = [];

            i = 0;
            length = constraints.length || 100;

            if (constraints.values) {
              for (; i < length; i++) {
                result.push(constraints.values[randomGenerator("number", { lessThan: constraints.values.length, floor: true})]);
              }
            } else {
              arrayType = constraints.arrayType || "string";
              arrayConstraints = constraints.arrayConstraints || {};

              for (; i < length; i++) {
                result.push(randomGenerator(arrayType, arrayConstraints));
              }
            }
          }

          return result;
        },
        obj, typeOfProperty, propertyName, property, constraints, value;


      while (total--) {
        obj = {};

        for (propertyName in schema) {
          if (schema.hasOwnProperty(propertyName)) {
            property = schema[propertyName];
            typeOfProperty = toString.call(property);

            if (typeOfProperty === "[object String]") {
              obj[propertyName] = randomGenerator(property);
            } else if (typeOfProperty === "[object Function]") {
              obj[property] = property(obj);
            } else if (typeOfProperty === "[object Object]") {
              value = randomGenerator(property.type, property);

              if (property.unique) {
                if (uniques[propertyName] === undefined) {
                  uniques[propertyName] = {};
                }

                while (uniques[propertyName][value] !== undefined) {
                  value = randomGenerator(property.type, property);
                }

                uniques[propertyName][value] = true;
              }

              obj[propertyName] = value;
            } else if (typeOfProperty === "[object Array]") {
              obj[propertyName] = property[randomGenerator("number", { lessThan: property.length, floor: true })];
            }
          }
        }

        results.push(obj);
      }

      return results;
    }
  };

  if (typeof define === "function" && define.amd) {
    define(generator);
  } else if (typeof module === "function" && module.exports) {
    module.exports = generator;
  } else {
    window.mockDataGenerator = generator;
  }
}());